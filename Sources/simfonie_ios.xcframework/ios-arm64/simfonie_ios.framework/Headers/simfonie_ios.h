//
//  simfonie_ios.h
//  simfonie_ios
//
//  Created by Elena Georgiou on 15/11/2016.
//  Copyright © 2016 Wirecard Issuing Technologiess. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for simfonie_ios.
FOUNDATION_EXPORT double simfonie_iosVersionNumber;

//! Project version string for simfonie_ios.
FOUNDATION_EXPORT const unsigned char simfonie_iosVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <simfonie_ios/PublicHeader.h>
//#import "HMACHelper.h"
