read -p 'Enter Certificate Name: ' certName
read -p 'Enter Certificate Extension: ' certExt
cat $certName.$certExt
read -p "Extract the Public key from this certificat (y/n)?" choice
case "$choice" in
y|Y )
openssl x509 -outform der -in $certName.$certExt -out $certName.der
openssl x509 -inform der -in $certName.der -pubkey -noout > public_key.pem
cat public_key.pem
echo "Public Key Extracted Successfully!";;
n|N )
echo "Extraction Canceled!";;
* ) echo "Extraction Canceled!";;
esac

